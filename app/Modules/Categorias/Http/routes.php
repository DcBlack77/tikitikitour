<?php

Route::group(['middleware' => 'web', 'prefix' => 'categorias', 'namespace' => 'App\\Modules\Categorias\Http\Controllers'], function()
{
    Route::get('/', 'CategoriasController@index');
});
