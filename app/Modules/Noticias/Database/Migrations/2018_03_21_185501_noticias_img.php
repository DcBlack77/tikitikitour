<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoticiasImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias_img', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('noticias_id')->unsigned();
			$table->string('archivo', 200);
			$table->string('descripcion', 200);
			$table->string('leyenda', 200);
			$table->string('tamano', 12);

			$table->foreign('noticias_id')
				->references('id')->on('noticias')
				->onDelete('cascade')->onUpdate('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
