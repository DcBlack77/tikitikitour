<?php

Route::group(['middleware' => 'web', 'prefix' => 'noticias', 'namespace' => 'App\\Modules\Noticias\Http\Controllers'], function()
{
    Route::get('/', 'NoticiasController@index');
});
